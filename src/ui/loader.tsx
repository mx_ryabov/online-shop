import * as React from 'react';
import styled, { keyframes } from 'styled-components';

const ldsEllipsis1 = keyframes`
    0% {
        transform: scale(0);
    }
    100% {
        transform: scale(1);
    }
`;

const ldsEllipsis2 = keyframes`
    0% {
        transform: translate(0, 0);
    }
    100% {
        transform: translate(24px, 0);
    }
`;

const ldsEllipsis3 = keyframes`
    0% {
        transform: scale(1);
    }
    100% {
        transform: scale(0);
    }
`;

const LoaderContainer = styled.div`
    width: 100%;
    height: 100vh;
    position: absolute;
    top: 0;
    left: 0;
    display: flex;
    justify-content: center;
    padding-top: 200px;
`;

const EllipsisContainer = styled.div`
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;

    & div {
        position: absolute;
        top: 33px;
        width: 13px;
        height: 13px;
        border-radius: 50%;
        background: #333;
        animation-timing-function: cubic-bezier(0, 1, 1, 0);

        &:nth-child(1) {
            left: 8px;
            animation: ${ldsEllipsis1} 0.6s infinite;
        }
        &:nth-child(2) {
            left: 8px;
            animation: ${ldsEllipsis2} 0.6s infinite;
        }
        &:nth-child(3) {
            left: 32px;
            animation: ${ldsEllipsis2} 0.6s infinite;
        }
        &:nth-child(4) {
            left: 56px;
            animation: ${ldsEllipsis3} 0.6s infinite;
        }
    }
`;

const Loader: React.FC = () => {
    return (
        <LoaderContainer>
            <EllipsisContainer>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </EllipsisContainer>
        </LoaderContainer>
    );
};

export default Loader;
