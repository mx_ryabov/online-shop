import config from './configure';
import axios from 'axios';

async function postRequest(url: string, data: any) {
    return axios({
        method: 'post',
        url: config.urlServer + url,
        data: data,
        withCredentials: true,
    })
        .then((res) => {
            console.log(res);

            return res;
        })
        .catch((err) => {
            console.log('Error', new FetchError(err.response.status, err.response.data.error));
            return err.response;
        });
}

async function getRequest(url: string, params: any) {
    let url_fetch = config.urlServer + url + '?';
    Object.keys(params).forEach((key: string) => (url_fetch += `${key}=${params[key]}&`));
    url_fetch = url_fetch.slice(0, -1);

    return axios
        .get(url_fetch, {})
        .then((res) => {
            return res;
        })
        .catch((err) => {
            console.log('Error', err.response);
            return err.response;
        });
}

export class FetchError extends Error {
    constructor(public status: number, public message: string) {
        super();
    }
}

export { postRequest, getRequest };
