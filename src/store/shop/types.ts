function updateLocaleStorage(target: any, key: string, descriptor: PropertyDescriptor) {
    return {
        value: function (...args: any[]) {
            console.log(key, target);

            let result = descriptor.value.apply(this, args);
            localStorage.setItem('cart', JSON.stringify(Array.from(result.map)));

            return result;
        },
    };
}

export class OrderMap {
    private map: Map<string, number>;

    constructor(props?: [string, number][]) {
        this.map = new Map<string, number>(props);
    }

    @updateLocaleStorage
    addItem(key: string): OrderMap {
        this.map.set(key, (this.map.get(key) || 0) + 1);
        return new OrderMap(Array.from(this.map));
    }

    @updateLocaleStorage
    removeItem(key: string): OrderMap {
        const newValue: number = (this.map.get(key) || 0) - 1;
        if (newValue > 0) {
            this.map.set(key, newValue);
        } else {
            this.map.delete(key);
        }
        return new OrderMap(Array.from(this.map));
    }

    @updateLocaleStorage
    clearItem(key: string): OrderMap {
        this.map.delete(key);
        return new OrderMap(Array.from(this.map));
    }

    getOrderItemsSum(): number {
        const values: number[] = Array.from(this.map.values());
        const itemsCount: number =
            values.length === 0 ? 0 : values.reduce((acc: number, currentValue: number) => acc + currentValue);
        return itemsCount;
    }

    getOrderItemCount(key: string): number {
        return this.map.get(key) || 0;
    }

    @updateLocaleStorage
    clearCart(): OrderMap {
        return new OrderMap();
    }

    getTotal(): number {
        const keys: string[] = Array.from(this.map.keys());
        if (keys.length === 0) return 0;
        return Number(
            keys
                .map((key: string) => +key.split('|$|')[1] * (this.map.get(key) || 1)) // вытягиваем из них прайс, превращаем в число, и умножаем на количество
                .reduce((acc: number, currentValue: number) => acc + currentValue) // считаем и выводим сумму
                .toFixed(2), // округляем до сотых
        );
    }

    @updateLocaleStorage
    clearTrashProducts(actualProductMap: Map<string, IProduct>): OrderMap {
        for (let key of this.map.keys()) {
            if (!actualProductMap.has(key)) this.map.delete(key);
        }
        return this;
    }

    toArray(): [string, number][] {
        return Array.from(this.map);
    }
}

export interface IProduct {
    name: string;
    price: number;
    image: string;
}

export interface ShopState {
    fetching: boolean;
    products: Map<string, IProduct>;
    cart: OrderMap;
    dealers: string[];
    error: Object | undefined;
}

export const START_FETCH = 'START_FETCH';
export const FETCH_ERROR = 'FETCH_ERROR';

export const GET_PRODUCTS = 'GET_PRODUCTS';
export const GET_DEALERS = 'GET_DEALERS';
export const ADD_PRODUCT_ITEM_TO_CART = 'ADD_PRODUCT_ITEM_TO_CART';
export const REMOVE_PRODUCT_ITEM_FROM_CART = 'REMOVE_PRODUCT_ITEM_FROM_CART';
export const DELETE_PRODUCT_FROM_CART = 'DELETE_PRODUCT_FROM_CART';
export const CLEAR_CART = 'CLEAR_CART';
export const LOAD_STORAGE_DATA = 'LOAD_STORAGE_DATA';

export interface StartFetchAction {
    type: typeof START_FETCH;
}

interface FetchActionError {
    type: typeof FETCH_ERROR;
    payload?: { error: Object | undefined };
}

interface GetProducts {
    type: typeof GET_PRODUCTS;
    payload: IProduct[];
}

interface GetDealers {
    type: typeof GET_DEALERS;
    payload: string[];
}

interface AddProductItemToCart {
    type: typeof ADD_PRODUCT_ITEM_TO_CART;
    payload: string;
}

interface RemoveProductItemFromCart {
    type: typeof REMOVE_PRODUCT_ITEM_FROM_CART;
    payload: string;
}

interface DeleteProductFromCart {
    type: typeof DELETE_PRODUCT_FROM_CART;
    payload: string;
}

interface ClearCart {
    type: typeof CLEAR_CART;
}

interface LoadStorageData {
    type: typeof LOAD_STORAGE_DATA;
    payload: OrderMap;
}

export type ShopActionTypes =
    | StartFetchAction
    | FetchActionError
    | RemoveProductItemFromCart
    | GetProducts
    | GetDealers
    | AddProductItemToCart
    | DeleteProductFromCart
    | LoadStorageData
    | ClearCart;
