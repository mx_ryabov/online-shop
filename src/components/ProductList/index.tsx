import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../store';
import { addProductItemToCart } from '../../store/shop/actions';
import { ShopState } from '../../store/shop/types';
import Loader from '../../ui/loader';
import ProductCard from '../ProductCard.tsx';
import { Col, ProductListContainer, Row } from './styled';

const ProductList: React.FC = () => {
    const { products, fetching, cart } = useSelector<AppState, ShopState>((state) => state.shop);
    const dispatch = useDispatch();

    return (
        <React.Fragment>
            {fetching && <Loader />}
            <ProductListContainer>
                <Row>
                    {!fetching &&
                        Array.from(products).map((prod) => (
                            <Col key={prod[0]}>
                                <ProductCard
                                    {...prod[1]}
                                    onBuy={() => {
                                        dispatch(addProductItemToCart(prod[0]));
                                    }}
                                    selectedCount={cart.getOrderItemCount(prod[0])}
                                />
                            </Col>
                        ))}
                </Row>
            </ProductListContainer>
        </React.Fragment>
    );
};

export default ProductList;
