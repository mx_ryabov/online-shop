import * as React from 'react';
import configure from '../../configure';
import { IProduct } from '../../store/shop/types';
import { CardContainer, CartButton, Price } from './styled';

interface IProductCardProps extends IProduct {
    selectedCount?: number;
    onBuy: () => void;
}

const ProductCard: React.FC<IProductCardProps> = ({ image, name, price, selectedCount, onBuy }) => {
    return (
        <CardContainer>
            <img src={configure.urlServer + image} alt="" />
            <p>{name}</p>
            <CartButton selectedCount={selectedCount} onClick={onBuy}>
                Buy
            </CartButton>
            <Price>${price}</Price>
        </CardContainer>
    );
};

function areEqual(prevProps: IProductCardProps, nextProps: IProductCardProps) {
    return prevProps.name === nextProps.name && prevProps.selectedCount === nextProps.selectedCount;
}

export default React.memo(ProductCard, areEqual);
