import styled from 'styled-components';
import { Button, OrangeCounterStyles } from '../../ui';

export const CardContainer = styled.div`
    background: #ffffff;
    box-shadow: 3px 3px 50px rgba(141, 141, 141, 0.2);
    border-radius: 16px;
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    padding: 10px 10px;
    margin-bottom: 15px;

    img {
        width: 100%;
        min-height: 140px;
    }
    img + p {
        width: 100%;
        margin-bottom: 15px;
    }

    @media only screen and (min-width: 768px) {
        padding: 15px;
    }
`;

export const CartButton = styled(Button)<{ selectedCount?: number }>`
    width: 50%;
    position: relative;

    &::after {
        content: 'x${(props) => props.selectedCount}';
        display: ${(props) => (props.selectedCount ? 'block' : 'none')};
        ${OrangeCounterStyles}
    }
`;

export const Price = styled.p`
    width: 50%;
    font-size: 18px;
    line-height: 40px;
    color: #333333;
    font-family: 'Open Sans ExtraBold';
    text-align: right;
`;
