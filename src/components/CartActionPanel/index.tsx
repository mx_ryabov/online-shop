import * as React from 'react';
import { useDispatch } from 'react-redux';
import { clearCart } from '../../store/shop/actions';
import { DeleteButton } from '../../ui';
import { PanelContainer, Title, OrangeButton } from './styled';

const CartActionPanel: React.FC = () => {
    const dispatch = useDispatch();
    return (
        <PanelContainer>
            <Title>Order List</Title>
            <section>
                <OrangeButton onClick={() => alert('This button is just for beauty :)')}>Buy</OrangeButton>
                <DeleteButton onClick={() => dispatch(clearCart())} />
            </section>
        </PanelContainer>
    );
};

export default CartActionPanel;
