import styled from 'styled-components';

export const CartListContainer = styled.div`
    padding: 15px;
    max-width: 808px;
    margin: 0 auto;
`;
