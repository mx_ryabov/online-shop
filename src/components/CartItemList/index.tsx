import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from '../../store';
import { addProductItemToCart, deleteProductFromCart, removeProductItemFromCart } from '../../store/shop/actions';
import { IProduct, ShopState } from '../../store/shop/types';
import Loader from '../../ui/loader';
import Cartitem from '../CartItem';
import { CartListContainer } from './styled';

const CartItemList: React.FC = () => {
    const { products, cart, fetching } = useSelector<AppState, ShopState>((state) => state.shop);
    const dispatch = useDispatch();
    console.log(cart);

    const cartItems = cart.toArray().map((kv: [string, number]) => {
        const prod: IProduct | undefined = products.get(kv[0]);
        if (prod) return { ...prod, count: kv[1], key: kv[0] };
    });

    return (
        <React.Fragment>
            {fetching && <Loader />}
            <CartListContainer>
                {!fetching && cartItems.length > 0 ? (
                    cartItems.map(
                        (item) =>
                            item && (
                                <Cartitem
                                    {...item}
                                    onIncrement={() => dispatch(addProductItemToCart(item.key))}
                                    onDecrement={() => dispatch(removeProductItemFromCart(item.key))}
                                    onRemove={() => dispatch(deleteProductFromCart(item.key))}
                                />
                            ),
                    )
                ) : (
                    <p>Empty cart...</p>
                )}
            </CartListContainer>
        </React.Fragment>
    );
};

export default CartItemList;
