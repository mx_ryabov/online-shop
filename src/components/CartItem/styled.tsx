import styled from 'styled-components';
import { Button, DeleteButton } from '../../ui';

export const CartItemContainer = styled.div`
    width: 100%;
    margin-bottom: 20px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;

    ${DeleteButton} {
        display: none;
    }

    @media only screen and (min-width: 500px) {
        ${DeleteButton} {
            display: flex;
        }
    }
`;

export const Info = styled.div`
    display: flex;
    flex-direction: row;

    img {
        width: 80px;
        height: 80px;
        background: #ffffff;
        box-shadow: 3px 3px 50px rgba(141, 141, 141, 0.2);
        border-radius: 16px;
        padding: 8px;
        margin-right: 18px;
    }

    & > div {
        display: flex;
        flex-direction: column;
        justify-content: center;
        width: 100px;
        p {
            font-size: 20px;

            &:first-of-type {
                margin-bottom: 8px;
            }
            &:last-of-type {
                font-family: 'Open Sans ExtraBold';
            }
        }
    }
`;

export const Actions = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;

    span {
        margin: 0 10px;
        font-size: 22px;
    }

    ${Button} {
        font-size: 26px;
    }
`;
